import frappe
from frappe import _, get_doc
from frappe.desk.reportview import get_count as get_filtered_count
from frappe.desk.reportview import get_form_params


@frappe.whitelist(allow_guest=True)
def get_list():
	args = get_form_params()
	docs = frappe.get_list(**args)

	frappe.local.form_dict.pop("limit_page_length", None)

	return {
		"docs": docs,
		"length": get_filtered_count(),
	}
